
/* 
TODO:  structure this better.  Right now we execute this code immediately when the browser parses the script tag in index.
We should probably make it happen when the page loads
*/


var canvas = document.getElementById('canvas');
var playerOne, player;




var MOVE_PIXELS = 10;


function main (){


	if (canvas.getContext) {
		var ctx = canvas.getContext('2d');		
	}

	playerOne = player(ctx, 20, 380);
	playerOne.draw();
	document.getElementById('canvas').addEventListener('click', playerOne.moveLeft, true);


}
window.addEventListener("load", main, true);



player = function (ctx, x, y){
	return {
		ctx: ctx,
		x: x,
		y: y,
		/* TODO: bryan.  Figure out the best way to structure this */
		/* TODO: make this a method decorator.
 		animate: function (fn){
			this.clear();
			//call the function here
			this.draw();
		}
		*/


		moveLeft: function(){
			//TODO: Fix the scope error here... calling this.clear() gets throws an 
			//uncaught type error "Canvas element has no clear method"
			ctx.clearRect(this.x, this.y, 100, 100);
			this.x = this.x - MOVE_PIXELS;
			ctx.fillRect(this.x, this.y ,100,100);
		},
		moveRight: function(){
			this.clear();
			this.x = this.x + MOVE_PIXELS;
			this.draw();
		},


		draw: function(){
			this.ctx.fillRect(this.x, this.y ,100,100);	
		},

		clear: function(){
			this.ctx.clearRect(this.x, this.y, 100,100);
		}



	}



}
